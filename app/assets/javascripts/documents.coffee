# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
	$('#new_document').validate
		ignore: []
		rules:
			'document[attachment_id]':
				required: true
			'document[book_attributes][title]':
				minlength: 10
			'document[notebook_attributes][student_name]':
				minlength: 5
			'document[notebook_attributes][teacher_name]':
				minlength: 5
			'document[summary_attributes][teacher_name]':
				minlength: 5
			'document[summary_attributes][description]':
				required: false
		messages:
			'document[course_id]':
				required: 'لم تقم بإدخال المساق المتعلق بالملف'
			'document[attachment_id]':
			# Examform
				required: 'لم تقم برفع الملف بعد'
			'document[examform_attributes][examtype_id]':
				required: 'لم تقم بإدخال نوع الإمتحان'
			'document[examform_attributes][semester_id]':
				required: 'لم تقم بإدخال الفصل الدراسي'
			'document[examform_attributes][fall_id]':
				required: 'لم تقم بإدخال سنة الملف'
				# Notebook
			'document[notebook_attributes][examtype_ids][]':
				required: 'لم تقم بإدخال نوع الإمتحان'
			'document[notebook_attributes][semester_id]':
				required: 'لم تقم بإدخال الفصل الدراسي'
			'document[notebook_attributes][fall_id]':
				required: 'لم تقم بإدخال سنة الملف'
			'document[notebook_attributes][student_name]':
				required: 'لم تقم بادخال اسم المدرس المنقول منه الدفتر'
				minlength: 'الحد الأدني 5 احرف'
			'document[notebook_attributes][teacher_name]':
				required: 'لم تقم بادخال اسم الطالب من كتب الدفتر'
				minlength: 'الحد الأدني 5 احرف'
				# Summary
			'document[summary_attributes][examtype_ids][]':
				required: 'لم تقم بإدخال نوع الإمتحان'
			'document[summary_attributes][student_name]':
				required: 'لم تقم بادخال اسم المدرس صاحب الشرح'
				minlength: 'الحد الأدني 5 احرف'
				# Book
			'document[book_attributes][title]':
				required: 'لم تقم بإدخال عنوان الكتاب. مثال. مثال: Digital Design With An Introduction to the Verilog HDL FIFTH EDITON'
				minlength: 'الحد الأدني 10 احرف'

		highlight: (element, errorClass, validClass) ->
			$(element).closest('.form-group').removeClass('has-success').addClass 'has-error'
			if $(element).attr('id') == 'document_attachment_id'
				$('.upload-form-group').addClass 'has-error'
		unhighlight: (element, errorClass, validClass) ->
		  $(element).closest('.form-group').removeClass('has-error').addClass 'has-success'
		success: (label) ->
		  $(label).closest('form').find('.valid').removeClass 'invalid'
		errorPlacement: (error, element) ->
		  $(element).closest('.form-group').find('.help-block').html error.text()
				if $(element).attr('id') == 'document_attachment_id'
					$('.upload-form-group').find('.help-block').html error.text()
	$('#document_course_id').select2 dir: 'rtl', width: '70%%'
	$("select[id$='attributes_fall_id']").select2 dir: 'rtl', minimumResultsForSearch: Infinity, width: '100%'
	$('#course_id').select2 dir: 'ltr', width: '90%'
	$('.btn-group').attr("data-toggle", "buttons")
	$('.col-md-7').append('<span class="help-block"></span>')
	$('#new_document').change ->
	  $('#new_document').valid
$(document).ready(ready)
$(document).on('page:load', ready)
