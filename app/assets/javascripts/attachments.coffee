# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  $('#new_attachment').fileupload
    add: (e, data) ->
      types = /(\.|\/)(pdf)$/i
      limitConcurrentUploads: 1
      maxNumberOfFiles: 1
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $(tmpl("template-upload", file))
        $('.upload').hide()
        $('.upload-document').append(data.context)
        data.submit()
      else
        alert("يجب أن يكون الملف بصيغة PDF")
    progress: (e, data) ->
      $('.alert').remove()
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.progress-bar').css('width', progress + '%')
        data.context.find('.progress-bar').html(progress + '%')

$(document).ready(ready)
$(document).on('page:load', ready)