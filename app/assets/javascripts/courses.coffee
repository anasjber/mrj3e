# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  getQueryVariable = (variable) ->
    query = window.location.search.substring(1)
    vars = query.split('&')
    i = 0
    while i < vars.length
      pair = vars[i].split('=')
      if pair[0] == variable
        return pair[1]
      i++
    false
  $('#documents-table').on 'click', '#showDocument', (e) ->
    e.preventDefault()
    url = "/documents/" + $(this).data('id')
    $.getJSON(url, (data) ->
      $('.modal-body').html data.render
      )
  if window.location.href.indexOf('show') != -1
    url = "/documents/" + $('#document_id').val()
    $.getJSON(url, (data) ->
      $('a[data-target="' + data.type).click()
      setTimeout (->
        $('#document-'+data.id).addClass('active')
        ), 500
      $('.modal-body').html data.render
      )
    $('#showDocumentInfo').modal 'show'
  $('a[data-toggle="tab"]').on 'show.bs.tab', (e) ->
    $('.table-loader').fadeIn(100).show()
    $('#documents-table').html ''
    $('#loadmore-btn').show()
    url = $(this).attr('href')
    $('#element_type').val $(this).data('target')
    $.getJSON(url, (data) ->
      i = 0
      len = data.documents.length
      $('#documents-table').append data.header
      while i < len
        $('#documents-table tbody').append data.documents[i].render
        i++
      )
      .done(->
        $('.table-loader').fadeOut(100).hide()
      ).error(->
        $('#documents-table tbody').append('حدث خطأ')
      )
  $('#load-more').on 'click', '#loadmore-btn', (e) ->
    $('.loadmore-loader').show()
    $('#loadmore-btn').hide()
    document_type = $('#element_type').val()
    course = $('#course').val()
    document_ids = []
    i = 0
    $('tr').each (index) ->
      document_ids[i++] = $(this).attr('id')
    $.ajax '/load_more',
        type: 'POST'
        cache: false
        dataType: 'JSON'
        data: document_ids: document_ids, element_type: document_type, id: course
        success: (data) ->
          i = 0
          len = data.documents.length
          $('#documents-table').append data.header
          while i < len
            $('#documents-table tbody').append data.documents[i].render
            i++
          if data.documents.length == 0
            $('#loadmore-btn').hide()
          else
            $('#loadmore-btn').show()
          $('.loadmore-loader').hide()
  $('.nav-sidebar li').first().addClass('active')
  $('#courseSearch').autocomplete
    serviceUrl: 'search'
    minChars: 2
    type: 'POST'
    showNoSuggestionNotice: true
    noSuggestionNotice: "عذراً بحثك لم يطابق المساقات المتوفرة في قاعدة البيانات"
    onSelect: (suggestion) ->
      window.open(window.location.href = '../' + suggestion.slug, '_self')
    onSearchStart: ->
      $(this).append('loading')
$(document).ready(ready)
$(document).on('page:load', ready)
