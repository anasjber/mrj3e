class Attachment < ActiveRecord::Base
	mount_uploader :file, AttachmentUploader
	validates_presence_of :file
	validate :unique
	has_one :document
	def unique
		if Attachment.where(md5: self.md5, used: true).count > 0
			errors.add(:md5, "عذراً الملف مكرر")
		end
	end
end