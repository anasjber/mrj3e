class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable :registerable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :invitable
  has_many :documents
	extend FriendlyId
	friendly_id :username, use: [:slugged, :finders]
	validates_presence_of :first_name, :last_name, :username
	def full_name
		"#{self.first_name} #{self.last_name}" 
	end
	def owner?(document_id)
		if self == Document.find(document_id).user
			return true
		end
	end
end
