class Notebook < ActiveRecord::Base
	# Association
	has_one :document, foreign_key: "element_id"
	has_one :course, through: :document
	has_one :attachment, through: :document
	has_one :user, through: :document
	belongs_to :semester
	belongs_to :examtype, foreign_key: "examtype_ids"

	#Validate
	validates_presence_of :examtype_ids, :semester_id, :student_name
	validates_length_of :student_name, :teacher_name, maximum: 17

	serialize :examtype_ids, Array
	def examtypes
		Examtype.where(id: self.examtype_ids).map {|e| e.examtype }
	end
end
