class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    can :read, [Document, Course]
    can :download, [Document]
    can :short_link, [Document]
    if user.publisher?
      can :create, [Document, Attachment]
    end
  end
end
