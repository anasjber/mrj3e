class Summary < ActiveRecord::Base
	# Association
	has_one :document, foreign_key: "element_id"
	has_one :course, through: :document
	has_one :attachment, through: :document
	has_one :user, through: :document
	belongs_to :semester
	belongs_to :fall
	belongs_to :examtype
	
	# Validate
	validates_presence_of :examtype_ids, :student_name
	validates_length_of :student_name, maximum: 17
	validates_length_of :description, maximum: 40

	serialize :examtype_ids, Array
	def examtypes
		Examtype.where(id: self.examtype_ids).map {|e| e.examtype }
	end
end
