class Book < ActiveRecord::Base
	has_one :document, foreign_key: 'element_id'
	has_one :course, through: :document
	has_one :attachment, through: :document
	has_one :user, through: :document

	validates_presence_of :title
	validates_length_of :title, maximum: 90, minimum: 7 
end
