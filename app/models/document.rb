class Document < ActiveRecord::Base
	include PublicActivity::Common
	include ActiveModel::ForbiddenAttributesProtection
	before_destroy :destory_element
	# Association
	belongs_to :attachment, dependent: :destroy
	belongs_to :user
	belongs_to :course, counter_cache: true
	belongs_to :examform, foreign_key: "element_id"
	belongs_to :notebook, foreign_key: "element_id"
	belongs_to :book, foreign_key: "element_id"
	belongs_to :summary, foreign_key: "element_id"
	# Accepts Nested Attribute
	accepts_nested_attributes_for :examform, :notebook, :book, :summary
	# Validate
	validates :attachment_id, :course_id, :element_type, :user_id, presence: true
	validates :attachment_id, uniqueness: true
	validates :element_type, inclusion: {in: %w{ examform notebook book summary }, message: "%{value} ليست قيمة صحيحة لنوع الملف"}
	# Get Document Element Object
	def element
		(self.element_type).classify.constantize.find(self.element_id)
	end
	def destory_element
		self.element.destroy
	end
end
