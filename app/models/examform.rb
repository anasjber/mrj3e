class Examform < ActiveRecord::Base
	# Association
	has_one :document, foreign_key: "element_id"
	has_one :course, through: :document
	has_one :attachment, through: :document
	has_one :user, through: :document
	belongs_to :semester
	belongs_to :fall
	belongs_to :examtype
	# Validate
	validates_presence_of :examtype_id, :fall_id, :semester_id
end