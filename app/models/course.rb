class Course < ActiveRecord::Base
	attr_readonly :documents_count
	has_many :documents
	# Friendly ID
	extend FriendlyId
	friendly_id :symbol, use: [:slugged, :finders]
	
	validates_presence_of :name, :symbol

	def full_formula
		"[#{self.symbol}] #{self.name}"
	end
end
