json.header render('documents/table_header', formats: [:html])
json.documents @documents do |document|
	json.id document.id
	json.render render(document, formats: [:html])
end