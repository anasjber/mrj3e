json.suggestions @results do |course|
	json.value course.full_formula
	json.slug course.slug
end