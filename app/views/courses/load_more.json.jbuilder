json.documents @documents do |document|
	json.render render(document, formats: [:html])
end