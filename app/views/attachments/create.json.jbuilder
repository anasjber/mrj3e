json.attachment do |attachment|
	json.id @attachment.id 
	json.thumb @attachment.document.cover_thumb.url
end
if @attachment.errors.any?
	json.status 'fail'
else
	json.status 'success'
end