class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :get_course_document_type, :load_more, :show_document]
  def show
  	@documents = @course.documents.where(element_type: 'examform').order("download_count DESC").limit(10)
  end
  def get_course_document_type
    @documents = @course.documents.where(element_type: params[:element_type]).order("download_count DESC").limit(10)
  end
  def load_more
    @document_ids = params[:document_ids].map {|id| id.match(/[^-]+$/).to_s.to_i }
    @documents = @course.documents.where(element_type: params[:element_type]).where.not(id: @document_ids).order("download_count DESC").limit(10)
  end
  def search
    @results = Course.where("symbol like :search or name like :search", search: "%#{params[:query]}%").where("documents_count > 0")
  end
  def show_document
    @documents = @course.documents.where(element_type: 'examform').order("download_count DESC").limit(10)
    @document = Document.find(params[:document_id])
    respond_to do |format|
      format.html {render action: :show}
    end
  end
  private
  def set_course
    @course = Course.find(params[:id])
  end

end