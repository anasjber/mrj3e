class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up){ |u| u.permit(:first_name, :last_name, :username, :password, :password_confrimation, :email)}
    devise_parameter_sanitizer.for(:sign_in){ |u| u.permit(:email)}
    devise_parameter_sanitizer.for(:account_update){ |u| u.permit(:first_name, :last_name, :email, :password)}
	  # Only add some parameters
	  devise_parameter_sanitizer.for(:accept_invitation).concat [:first_name, :last_name, :username]
  end
  def is_admin?
  	unless current_user.admin?
  		redirect_to root_path
  		flash[:notice] = 'لا تملك الصلاحية لإرسال دعوات.'
  	end
  end
end
