class AttachmentsController < ApplicationController
  def create
    @attachment = Attachment.create(attachment_params)
    respond_to do |format|
    	format.js
    end
  end

  private

  def attachment_params
    params.require(:attachment).permit(:file, :file_size)
  end
end
