class InvitationsController < Devise::InvitationsController
  before_action :is_admin?, only: [:new, :create]
end