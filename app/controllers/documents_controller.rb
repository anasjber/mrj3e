class DocumentsController < ApplicationController
  load_and_authorize_resource
  before_action :set_document, only: [:show, :download, :short_link]
  def index
  	@documents = Document.all
  end

  def show

  end

  def new
  	@document = Document.new
    if params[:element_type] == "examform"
      @examform = @document.build_examform
    elsif params[:element_type] == "notebook"
      @notebook = @document.build_notebook
    elsif params[:element_type] == "summary"
      @summary = @document.build_summary
    elsif params[:element_type] == "book"
      @book = @document.build_book
    end
    render template: "/documents/new/#{params[:element_type]}"
  end
  def create
  	@document = current_user.documents.create(document_params)
    if @document.save
      Attachment.find(@document.attachment_id).update_attribute(:used, true)
      redirect_to course_path(Course.friendly.find(@document.course_id))
      flash[:success] = 'تم حفط الملف بنجاح.'
    else
      redirect_to new_document_path(@document.element_type)
      flash[:error] = "حدث خطأ أثناء حفظ الملف الرجاء المحاولة مرة اخرى. في حال إستمرار الخطأ الرجاء التواصل معنا"
    end
  end
  def download
    @document.increment!(:download_count)
    redirect_to @document.attachment.file.url
  end
  def short_link
    @course = @document.course
    redirect_to course_show_document_path(@course, @document)
  end
  private
  def set_document
    @document = Document.find(params[:id])
  end
  def document_params
    params.require(:document).permit(:user_id, :course_id, :attachment_id, :element_type,
      examform_attributes: [:fall_id, :examtype_id, :semester_id],
      notebook_attributes: [:semester_id, :student_name, :teacher_name, examtype_ids: []],
      book_attributes: [:title],
      summary_attributes: [:student_name, :description, examtype_ids: []])
  end
end
