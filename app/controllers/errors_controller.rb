class ErrorsController < ApplicationController
	def index
	end
  def not_found
  	render(:status => 404)
  end

  def internal_server_error
  	render(:status => 505)
  end
end
