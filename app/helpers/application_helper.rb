module ApplicationHelper
  def type_list
    @type_list = [["أسئلة سنوات", "examform"], ["دفتر", "notebook"], ["شرح/ملخص", "summary"], ["كتاب", "book"]]
  end
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end
  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do 
              concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
              concat(content_tag(:p, class: "text-center")do
                concat message
              end
                )
            end)
    end
    nil
  end
  def title(*parts)
    content_for(:title) { (parts << t(:site_name)).join(' - ') } unless parts.empty?
  end
end
