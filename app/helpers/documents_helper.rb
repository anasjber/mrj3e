module DocumentsHelper
	def document_type_count(type)
		Document.where(element_type: type, course_id: Course.find(params[:id]||params[:course_id])).count
	end
end
