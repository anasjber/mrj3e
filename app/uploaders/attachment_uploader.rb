# encoding: utf-8
class AttachmentUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  include CarrierWave::MimeTypes
  CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/
  process :set_content_type
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :cover do
    process :pdf_preview => [700, 2000]
    process :set_content_type_jpg
    def full_filename (for_file = model.logo.file)
      super.chomp(File.extname(super)) + '.jpg'
    end
  end

  version :cover_thumb do
    process :pdf_preview => [300, 2000]
    process :set_content_type_jpg
    def full_filename (for_file = model.logo.file)
      super.chomp(File.extname(super)) + '.jpg'
    end
  end
  
  def pdf_preview(width, height)
    # Most PDFs have a transparent background, which becomes black when converted to jpg.
    # To override this, we must create a white canvas and composite the PDF onto the convas.
    # Read in first page of PDF and resize ([0] -> read the first page only)
    image = ::Magick::Image.read("#{current_path}[0]").first
    image.resize_to_fit!(width, height)
    # Create a blank canvas
    canvas = ::Magick::Image.new(image.columns, image.rows) { self.background_color = "#FFF" }
    # Merge PDF thumbnail onto canvas
    canvas.composite!(image, ::Magick::CenterGravity, ::Magick::OverCompositeOp)
    # save as jpg
    canvas.write("jpg:#{current_path}")
    # Free memory allocated by RMagick which isn't managed by Ruby
    image.destroy!
    canvas.destroy!
  rescue ::Magick::ImageMagickError => e
    Rails.logger.error "Failed to create PDF thumbnail: #{e.message}"
    raise CarrierWave::ProcessingError, "ملف PDF غير صالح"
  end

  def extension_white_list
     %w(pdf)
  end
  process :save_md5_in_model
  
  protected

  def md5
    @md5 ||= Digest::MD5.hexdigest model.send(mounted_as).read.to_s
  end
  def save_md5_in_model
    model.md5 = md5
  end
  def set_content_type_jpg(*args)
    self.file.instance_variable_set(:@content_type, "image/jpeg")
  end  

end
