Rails.application.routes.draw do
  get 'errors/not_found'

  get 'errors/internal_server_error'

  # Daynamic Errors Pages
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
  root 'documents#index'
  get '/sign_up', to: redirect('/')
  devise_for :users, :controllers => { :registrations => 'registrations', sessions: 'sessions', invitations: 'invitations'}, :path_names => {:sign_in => 'login', :sign_out => 'logout', edit: 'account', password: 'password', confirmation: 'confirmation'}, path: ''
  # Courses
  get ':id', to: 'courses#show', as: 'course', constraints: {id: /\w{2,4}\d{2,4}/}
  post 'load_more', to: 'courses#load_more', as: 'load_more'
  post 'search', to: 'courses#search', as: 'search'
  get ':id/:element_type.json', to: 'courses#get_course_document_type', as: 'get_course_document_type', constraints: {id: /\w{2,4}\d{2,4}/, element_type: /examform|notebook|summary|book/}
  get ':id/:document_id/show', to: 'courses#show_document', as: 'course_show_document', constraints: {id: /\w{2,4}\d{2,4}/, document_id: /\d+/}
  # Attachments
  resources :attachments, only: [:create, :new, :index]
  # Documents
  resources :documents, only: [:show, :index], constraints: {id: /\d+/} do
    member do
      get :download
    end
  end
  get '/go/:id', to: 'documents#short_link', as: 'document_short_link', constraints: {id: /\d+/}
  get '/add/:element_type', to: 'documents#new', as: 'new_document', constraints: {element_type: /examform|notebook|summary|book/}
  post '/documents', to: 'documents#create', as: 'create_document'
end