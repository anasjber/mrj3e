require 'rails_helper'


describe Document do
	# Presence
	it {should validate_presence_of(:course_id)}
	it {should validate_presence_of(:attachment_id)}
	it {should validate_presence_of(:user_id)}
	it {should validate_presence_of(:element_type)}
	# Uniqueness
	it {should validate_uniqueness_of(:attachment_id)}
	# Element Type Values
	it {should validate_inclusion_of(:element_type).in_array(%w(examform notebook book summary))}
	it {should_not validate_inclusion_of(:element_type).in_array(%w(examm blaah summ boo))}
	# Association
	it {should belong_to :examform}
	it {should belong_to :notebook}
	it {should belong_to :summary}
	it {should belong_to :book}
	# Accept Nested Attrbute
	it {should accept_nested_attributes_for(:summary)}
end