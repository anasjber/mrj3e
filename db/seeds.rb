require 'csv'

user = User.create! first_name: "Anas", last_name: "Tammam", username: "anasjber", email: "anas.jber@gmail.com", password: "11223344", password_confirmation: "11223344", admin: true, publisher: true
AdminUser.create!(email: 'admin@mrj3e.com', password: 'password', password_confirmation: 'password')

CSV.foreach('db/seeds/examtypes_seeds.txt') do |examtype|
	Examtype.create(examtype: examtype[0])
end
CSV.foreach('db/seeds/falls_seeds.txt') do |fall|
	Fall.create(fall: fall[0])
end
CSV.foreach('db/seeds/semesters_seeds.txt') do |semester|
	Semester.create(semester: semester[0])
end
CSV.foreach('db/seeds/courses_seeds.txt') do |course|
	Course.create(symbol: course[0], name: course[1].titleize)
end