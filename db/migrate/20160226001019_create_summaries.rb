class CreateSummaries < ActiveRecord::Migration
  def change
    create_table :summaries do |t|
    	t.string :examtype_ids, array: true, default: []
    	t.string :student_name
    	
      t.timestamps null: false
    end
  end
end
