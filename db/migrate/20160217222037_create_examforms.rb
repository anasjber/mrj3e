class CreateExamforms < ActiveRecord::Migration
  def change
    create_table :examforms do |t|
      t.references :examtype
      t.references :fall
      t.references :semester
      
      t.timestamps null: false
    end
  end
end
