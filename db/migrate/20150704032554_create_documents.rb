class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.references :user
      t.references :course
      t.references :attachment
      t.integer :download_count, default: 0
      t.integer :element_id
      t.string :element_type
      t.timestamps null: false
    end
  end
end