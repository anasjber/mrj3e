class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :document
      t.string :md5
      t.boolean :used, null: false, default: false
      t.timestamps null: false
    end
  end
end
