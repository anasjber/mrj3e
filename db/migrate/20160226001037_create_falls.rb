class CreateFalls < ActiveRecord::Migration
  def change
    create_table :falls do |t|
      t.string :fall

      t.timestamps null: false
    end
  end
end
