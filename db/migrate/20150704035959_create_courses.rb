class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.string :symbol
      t.string :slug, uniq: true
      t.integer :documents_count, null: false, default: 0
      t.timestamps null: false
    end
  end
end
