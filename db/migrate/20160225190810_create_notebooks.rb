class CreateNotebooks < ActiveRecord::Migration
  def change
    create_table :notebooks do |t|
      t.string :examtype_ids, array: true, default: []
      t.references :semester
      t.string :student_name
      t.string :teacher_name
      
      t.timestamps null: false
    end
  end
end
