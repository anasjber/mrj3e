class FixAttachmentColumnName < ActiveRecord::Migration
  def change
  	rename_column :attachments, :document, :file
  end
end
